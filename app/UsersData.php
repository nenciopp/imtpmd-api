<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersData extends Model
{
    public $timestamps = false;
    protected $fillable = ['active','uncompleted','completed','xp','level'];
}
