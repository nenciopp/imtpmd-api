<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\UsersData;
use DB;
use Auth;

class UserDataController extends Controller
{

    public function updateData(Request $request)
    {
      $current_user_id = Auth::user()->id;
      if(!$request->active){
        $request->active = '';
      }
      if(!$request->uncompleted){
        $request->uncompleted = '';
      }
      if(!$request->completed){
        $request->completed = '';
      }
      $userdata = UsersData::where('id', '=', $current_user_id)->update([
          'active' => $request->active,
          'uncompleted' => $request->uncompleted,
          'completed' => $request->completed,
          'xp' => $request->xp,
      ]);
      $response["users_data"] = $userdata;
      $response["success"] = 1;
      return response()->json($response);
    }

    public function getData(){
      $current_user_id = Auth::user()->id;
      $usersdata = UsersData::where('id', '=', $current_user_id)->get()->first();
      $usersdata->setAttribute('name', Auth::user()->name);
      $usersdata->setAttribute('email', Auth::user()->email);
      unset($usersdata->id);
      return $usersdata;
    }
}
