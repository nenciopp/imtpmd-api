<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('active')->default('');
            $table->string('uncompleted')->default('1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20');
            $table->string('completed')->default('');
            $table->integer('xp')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_data');
    }
}
